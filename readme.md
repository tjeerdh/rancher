# Setup Rancher
### Download [RancherOS](https://github.com/rancher/os) and create a bootable USB stick
### Bootup Rancher OS and type
```
wget -O go.sh https://gitlab.com/tjeerdh/rancher/raw/master/go.sh
chmod +x go.sh
./go.sh
./install.sh
```
### [Start Rancher Server](https://docs.rancher.com/rancher/v1.6/en/quick-start-guide/#rancher-server-tags)
```
sudo docker run -d --restart=unless-stopped -p 192.168.1.50:8080:8080 rancher/server:stable
```

### [Enable access controle](http://192.168.1.50:8080)

### [Add host to Rancher](http://192.168.1.50:8080/env/1a5/infra/hosts)

# Setup GitLab
### [Deploy GitLab container](https://docs.gitlab.com/omnibus/docker/#run-the-image)
update needed...
```
sudo docker run --detach \
    --hostname gitlab.quilzer.net \
    --publish 192.168.1.110:443:443 \
    --publish 192.168.1.110:80:80 \
    --publish 192.168.1.110:22:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab:Z \
    --volume /srv/gitlab/logs:/var/log/gitlab:Z \
    --volume /srv/gitlab/data:/var/opt/gitlab:Z \
    gitlab/gitlab-ce:latest
```

### [Enable HTTPS](https://docs.gitlab.com/omnibus/settings/nginx.html#enable-https)
#### Install certbot from [letsencrypt](https://letsencrypt.org/)
```
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot 
```
#### Request certificate
```
certbot certonly --standalone -d quilzer.net -d gitlab.quilzer.net -d host.quilzer.net -d www.quilzer.net
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Starting new HTTPS connection (1): acme-v01.api.letsencrypt.org
Obtaining a new certificate
Performing the following challenges:
tls-sni-01 challenge for quilzer.net
tls-sni-01 challenge for gitlab.quilzer.net
tls-sni-01 challenge for host.quilzer.net
tls-sni-01 challenge for www.quilzer.net
Waiting for verification...
Cleaning up challenges
Generating key (2048 bits): /etc/letsencrypt/keys/0000_key-certbot.pem
Creating CSR: /etc/letsencrypt/csr/0000_csr-certbot.pem

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at
   /etc/letsencrypt/live/quilzer.net/fullchain.pem. Your cert will
   expire on 2017-08-12. To obtain a new or tweaked version of this
   certificate in the future, simply run certbot again. To
   non-interactively renew *all* of your certificates, run "certbot
   renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

mv privkey.pem gitlab.quilzer.net.key
mv fullchain.pem gitlab.quilzer.net.crt
```

#### Install certificate
```
ssh rancher@192.168.1.50
docker container ls | grep gitlab
docker exec -i -t 8a1d4087b329 /bin/bash
echo 'external_url "https://gitlab.quilzer.net"' >> /etc/gitlab/gitlab.rb
mkdir -p /etc/gitlab/ssl
chmod 700 /etc/gitlab/ssl
cp gitlab.quilzer.net.key gitlab.quilzer.net.crt /etc/gitlab/ssl/
gitlab-ctl reconfigure
```
#### Backup GitLab
```
ssh rancher@192.168.1.50
docker container ls | grep gitlab
docker exec -t 8a1d4087b329 gitlab-rake gitlab:backup:create
sudo rsync -a /srv/gitlab/data/backups/*  bas@192.168.1.6:Backup/
```

#### [Restore GitLab](https://docs.gitlab.com/ce/raketasks/backup_restore.html#restore)
```
scp 1494746081_2017_05_14_gitlab_backup.tar  rancher@192.168.1.50:/srv/gitlab2/data/backups

docker container ls | grep gitlab
docker exec -i -t 8a1d4087b329 /bin/bash
cat /etc/gitlab/gitlab.rb | grep backup_path
gitlab-ctl reconfigure
```

Stop the processes that are connected to the database. Leave the rest of GitLab running:
```
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq
# Verify
gitlab-ctl status
```

Next, restore the backup, specifying the timestamp of the backup you wish to restore:
```
chmod 777 /var/opt/gitlab/backups/1494746081_2017_05_14_gitlab_backup.tar
gitlab-rake gitlab:backup:restore BACKUP=1494746081_2017_05_14
```

Restart and check GitLab:
```
gitlab-ctl start
gitlab-rake gitlab:check SANITIZE=true
```
